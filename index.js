const axios = require("axios");
const { Telegraf } = require("telegraf");

const bot = new Telegraf("5113655142:AAHALjTVmYyIpspHwwrh59SWt24FJ8uK2Iw");
let timerId = [];

bot.start((ctx) => ctx.reply("Welcome"));

bot.command("quit", (ctx) => {
  ctx.telegram.leaveChat(ctx.message.chat.id);
  ctx.leaveChat();
});

bot.command("stop", (ctx) => {
  while (timerId.length > 0) {
    clearInterval(timerId.pop);
  }
});

bot.help((ctx) => {
  ctx.reply(`Команды: 
Название одного города - получить данные о текущей погоде. 
/notice название города - подписаться на обновление погоды в данном городе (каждые 30 мин).
/stop - отписаться от всех уведомлений о погоде.
/help - список доступных команд`);
});

const getWeather = async (ctx, q) => {
    let err;
    const response = await axios
    .get("https://api.openweathermap.org/data/2.5/weather", {
      params: {
        q: q ? q : ctx.message.text,
        units: "metric",
        appid: "f29445accec9984e991a6a8348f9790f",
        lang: "ru",
      },
    }).catch(function (e) {
        ctx.reply("Ошибка! Город не найден: " + (q ? q : ctx.message.text));
        clearInterval(timerId.pop);
        err = e;
      });

      if(err){
        return {};
      }
      let weath_out = [];
      let warn_weath_out = [];
      let weather = await response.data.weather.filter((el) => {
        if ([2, 3, 5, 6, 7, 8].includes(+el.id.toString()[0])) {
          weath_out.push(el.description);
          if (![7, 8].includes(+el.id.toString()[0])) {
            warn_weath_out.push(el.description);
          }
          return true;
        }
        return false;
      });

      if (weath_out.length === 0) {
        weath_out.push("не ожидаются");
      }
      return {weath_out, warn_weath_out, response};   
}

const getPrognosWeather = async (ctx, lat, lon) => {
    let err;
    const response_warn = await axios
    .get("https://api.openweathermap.org/data/2.5/onecall", {
      params: {
        lat: lat,
        lon: lon,
        units: "metric",
        appid: "f29445accec9984e991a6a8348f9790f",
        lang: "ru",
        exclude: "current,minutely,daily"
      },
    }).catch(function (e) {
        ctx.reply("Ошибка! Город не найден: ");
        err = e;
      });

      if(err){
        return {};
      }
      let warn_weath_out = [];
      let alert_weath_out = [];
      let weather = await response_warn.data.hourly[1].weather.filter((el) => {
        if ([2, 3, 5, 6].includes(+el.id.toString()[0])) {       
            warn_weath_out.push(el.description);
          return true;
        }
        return false;
      });
      let alerts = await response_warn.data.alerts?.map((el) => {   
          alert_weath_out.push(el.description);
      });
      return {warn_weath_out, response_warn, alert_weath_out};   
}

const showWeather = (ctx, weath_out, response) => { 
    weath_out ? ctx.reply(response.data.name + ": " + response.data.main.temp + " °C" + ", Осадки: " + weath_out.join()) : "";
};

const showWarnWeather = (ctx, warn_weath_out, name) => {
    if(warn_weath_out.length > 0){
        ctx.reply(name + " - в ближайший 1ч ожидается: " + warn_weath_out.join());
    }
};

const showAlertWeather = (ctx, alert_weath_out, name) => {
  if(alert_weath_out.length > 0){
      ctx.reply("Внимание! " + name + " ожидается: " + alert_weath_out.join());
  }
};

bot.hears(/\/notice (.+)/, async function (ctx) {
  let q = ctx.match[1];
  let prev = {
    warn_weath_out : "",
    alert_weath_out : "",
    temp : -10000
  }

  let {weath_out, warn_weath_out, response} = await getWeather(ctx, q);
    if(response){
        prev.temp = response.data.main.temp;
        showWeather(ctx, weath_out, response);
        showWarnWeather(ctx, warn_weath_out, q);
    }

  let timer = setInterval(async function () {
    let {weath_out, response} = await getWeather(ctx, q);
    
    if(response){
      let lat = response.data.coord.lat;
      let lon = response.data.coord.lon;
      let {warn_weath_out, response_warn, alert_weath_out} = await getPrognosWeather(ctx, lat, lon);

      timerId.push(timer);
      //если температура изменилась с прошлого раза меньше, чем на 3 раза, то не уведомлять об изменениях температуры
      if(Math.abs(prev.temp - response.data.main.temp) > 3){
          prev.temp = response.data.main.temp;
          showWeather(ctx, weath_out, response);
        }
       
        if(response_warn){ 
          //уведомление об изменениях осадков 
          showWarnWeather(ctx, warn_weath_out, response.data.name); 
          // alert - Данные предупреждений о погоде из основных национальных систем предупреждения о погоде (гололед, сильный ветер, ураган, наводнение)       
          if(prev.alert_weath_out !== alert_weath_out.join()){
            prev.alert_weath_out = alert_weath_out.join();
            showAlertWeather(ctx, alert_weath_out, response.data.name);
          }
        }
      }
      else{
        clearInterval(timer);
      }      
  }, 60000*30);//30min
});

bot.on("text", async function (ctx) {
  if (!ctx.message.text.includes("/notice") && !ctx.message.text.includes("/help")) {
    let {weath_out, warn_weath_out, response} = await getWeather(ctx);
    if(response){
      showWeather(ctx, weath_out, response);
      showWarnWeather(ctx, warn_weath_out, response.data.name);
    }
  }
});

bot.launch();

// Enable graceful stop
process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));
